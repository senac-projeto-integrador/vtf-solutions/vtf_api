const knex = require("../dbConfig");
const bcrypt = require("bcrypt");

module.exports = {
  async users(req, res) {
    const usuarios = await knex("usuarios").select(
      "idU",
      "nome",
      "email",
      "adm",
      "created_at",
      "foto"
    );
    res.status(200).json({ erro: "false", msg: usuarios });
  },

  async createUser(req, res) {
    const { email, senha, nome, foto } = req.body;
    if (!email || !senha || !nome || !foto) {
      res.status(200).json({
        erro: "true",
        msg: "Enviar dados",
      });
      return;
    }
    try {
      const dados = await knex("usuarios").where({ email });
      if (dados.length) {
        res.status(200).json({ erro: "true", msg: "Email já cadastrado." });
        return;
      }
    } catch (error) {
      res.status(200).json({ erro: "true", msg: error.message });
    }

    const hash = bcrypt.hashSync(senha, 10);

    try {
      const newUser = await knex("usuarios").insert({
        email,
        nome,
        senha: hash,
        foto,
      });
      res.status(200).json({ erro: "false", id: newUser[0] });
    } catch (error) {
      res.status(200).json({ erro: "true", msg: error.message });
    }
  },

  async deleteUser(req, res) {
    const idU = req.params.id;

    try {
      await knex("usuarios").del().where({ idU });
      res.status(200).json({ erro: "false", msg: "Usuario Deletado" });
    } catch (error) {
      res.status(200).json({ erro: "true", msg: error.message });
    }
  },

  async updateUser(req, res) {
    const idU = req.params.id;
    const { newSenha } = req.body;
    if (!newSenha) {
      res.status(200).json({ erro: "true", msg: "Enviar Senha." });
      return;
    }
    try {
      const hash = bcrypt.hashSync(newSenha, 10);
      await knex("usuarios").update("senha", hash).where({ idU });
      res.status(200).json({ erro: "false", msg: "Senha alterada." });
    } catch (error) {
      res.status(200).json({ erro: "true", msg: error.message });
    }
  },
  async updateAdm(req, res) {
    const idU = req.params.idU;
    try {
      const usuario = await knex("usuarios").where({ idU });
      const adm =
        usuario[0].adm == false
          ? { valor: true, msg: "Usuario Agora é um ADM!" }
          : { valor: false, msg: "Usuario não é mais um ADM!" };
      await knex("usuarios").update("adm", adm.valor).where({ idU });
      res.status(200).json({ erro: "false", msg: adm.msg });
    } catch (error) {
      res.status(200).json({ erro: "true", msg: error.message });
    }
  },
};
