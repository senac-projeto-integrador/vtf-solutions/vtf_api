create database vtf_api;
use vtf_api;

create table usuarios(
    idU int auto_increment primary key,
    nome varchar(45) not null,
    email varchar(100) not null UNIQUE,
    senha varchar(120) not null,
    foto varchar(10000) not null,
    adm boolean not null DEFAULT FALSE,
    created_at timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
);

create table questoes(
    idQ int auto_increment primary key,
    idUser int not null,
    questao varchar(1000) not null,
    constraint fk_idUser foreign key (idUser) REFERENCES usuarios (idU),
    created_at timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
);

create table respostas(
    idR int auto_increment primary key,
    idUser int not null,
    constraint fk_idUseResp foreign KEY (idUser) REFERENCES usuarios (idU),
    idQuestao int not null,
    constraint fk_idQuestao foreign KEY (idQuestao) REFERENCES questoes (idQ) ON DELETE CASCADE,
    resposta varchar(1000) not null,
    created_at timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
);
