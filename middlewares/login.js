const jwt = require("jsonwebtoken");

module.exports = (req, res, next) =>{
    try{
        const token = req.headers.authorization.split(" ")[1];
        const decode = jwt.verify(token, process.env.JWT_KEY);
        req.usuario_id = decode.usuario_id;
        next();
    }catch(error){
        return res.status(200).send({erro: "true", msg: "Falha na autenticação"});
    };
}; 